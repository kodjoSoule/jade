package protocoles.voteCondorcet.agents;

public enum Resto {
    Pizza, Legumes, Pattate, Sushi, Moules, Gastro, Brasserie;
}